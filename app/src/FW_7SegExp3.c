/**
 * DOXYGEN COMMENTS
 *
 * @file   FW_7SegExp3.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "FW_7SegExp3.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

/**
	\fn  void Display_Inicializacion (void)
	\brief Inicializa las líneas de GPIO para el uso del Display
 	\param void
 	\return void
 */
void initDisplay7seg (void)
{
	//--------- SALIDAS DIGITALES (7 SEGMENTOS) ------//

	SetPINSEL ( EXPANSION0 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION1 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION2 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION3 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION4 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION5 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION6 , FUNCION_GPIO);

	SetDIR ( EXPANSION0 , SALIDA);
	SetDIR ( EXPANSION1 , SALIDA);
	SetDIR ( EXPANSION2 , SALIDA);
	SetDIR ( EXPANSION3 , SALIDA);
	SetDIR ( EXPANSION4 , SALIDA);
	SetDIR ( EXPANSION5 , SALIDA);
	SetDIR ( EXPANSION6 , SALIDA);

	// INICIALIZO EL 7SEG APAGADO
	SetPIN(EXPANSION0, ON);
	SetPIN(EXPANSION1, ON);
	SetPIN(EXPANSION2, ON);
	SetPIN(EXPANSION3, ON);

	SetPIN(EXPANSION5, OFF);
	SetPIN(EXPANSION6, OFF);

}

/**************************************************************************/

/**
	\fn  void Display_Barrrido (void)
	\brief Barrido de Display de la Expansión 3
 	\param void
 	\return void
 */
void Display_Barrrido (void)
{
	static uint8_t Digits[DIGIT_QUANTITY_FIX];
	static uint32_t index = 0;

	uint8_t specific_value = INVALID_VALUE;
	uint8_t index_aux = 0;

	setDIGIT(INVALID_VALUE); //Apago los dígitos

	MUX_7SEG;	//Multiplexo  (paso al siguiente digito)

	if(!index){ RST_7SEG; }		//Si el indice esta en 0, vuelvo el "cursor" al principio

	for(index_aux = 0; index_aux < DIGITOS; index_aux++)
		Digits[index_aux] = BufferDisplay[index_aux];

	specific_value = Digits[index];	//Cargo caracter por caracter los digitos que deseo imprimir, de acuerdo al indice

	setDIGIT(specific_value);	//Envio al Hard el valor del dígito

	index++;	//Aumento el indice

	if(index == DIGIT_QUANTITY)		//Si el indice supero la cantidad de digitos (n-1), vuelvo al principio
		index = 0;
}

/**************************************************************************/

void setDIGIT(char digito)
{
	switch(digito)		//Dependiendo el valor del dígito, genero la correspondiente salida de pines
	{
	case 0: DIGIT_VALUE0; break;

	case 1: DIGIT_VALUE1; break;

	case 2: DIGIT_VALUE2; break;

	case 3: DIGIT_VALUE3; break;

	case 4: DIGIT_VALUE4; break;

	case 5: DIGIT_VALUE5; break;

	case 6: DIGIT_VALUE6; break;

	case 7: DIGIT_VALUE7; break;

	case 8: DIGIT_VALUE8; break;

	case 9: DIGIT_VALUE9; break;

	default: DIGIT_NOTVALUE; break;
	}
}

/**************************************************************************/

/**
	\fn  void Display(int Val,char dsp)
	\brief Presentación de valores en el display de 3 dígitos
 	\param [in] val valor a mostrar
 	\param [in] dsp selección del sector del display
	\return void
 */
void Display(uint16_t Val, uint8_t dsp)
{
	if(Val > MAX_DISPLAY_VALUE)
		return;

	switch(dsp)
	{
	case DISPLAY_LEFT:
		BufferDisplay[0]=CENTENA(Val);
		BufferDisplay[1]=DECENA(Val);
		BufferDisplay[2]=UNIDAD(Val);
		break;
	case DISPLAY_RIGHT:
		BufferDisplay[3]=CENTENA(Val);
		BufferDisplay[4]=DECENA(Val);
		BufferDisplay[5]=UNIDAD(Val);
		break;
	}
}

/**************************************************************************/

void SetDisplayDigit(uint8_t Val, uint8_t digit)
{
	if(digit < DIGITOS)
		BufferDisplay[digit]=Val;
}
