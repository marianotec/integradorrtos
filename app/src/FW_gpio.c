/**
 * DOXYGEN COMMENTS
 *
 * @file   FW_gpio.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @fn			GPIO_SetOpenDrainMode()
 * @brief		Setea el Modo de la Salida asociada al  Pin
 *				Nota: Los pines P0[27] y P0[28] no pueden ser seteados ya que
 *				son siempre open-drain
 * @param[in]	portNum		Número de Puerto: Debe ser uno de los siguientes:
 *                          - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	pinNum		Número de Pin: Debe ser uno de los siguientes:
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in] 	odMode 	Modo de la Salida: Debe ser uno de los tipos
 *                          definidos en la enumeración PIN_OD_MODE
 * @return 		void
 **/

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "FW_LPC1769.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

void SetPINMODE( uint8_t port , uint8_t pin ,uint8_t modo)
{
	port = port * 2 + pin / 16;
	pin = ( pin % 16 ) * 2;
	PINMODE[ port ] = PINMODE[ port ] & ( ~ ( 3 << pin ) );
	PINMODE[ port ] = PINMODE[ port ] | ( modo << pin );
}

/********************************************************************************************************/

void SetDIR( uint8_t port , uint8_t pin , uint8_t dir )
{
	port = port * 8;

	GPIO[ port ] = GPIO[ port ] & ( ~ ( 1 << pin ) );
	GPIO[ port ] = GPIO[ port ] | ( dir << pin );
}

/********************************************************************************************************/

void SetPIN( uint8_t port , uint8_t pin , uint8_t estado )
{
	port = port * 8 + 5;

	GPIO[ port ] = GPIO[ port ] & ( ~ ( 1 << pin ) );
	GPIO[ port ] = GPIO[ port ] | ( estado << pin );
}

/********************************************************************************************************/

uint8_t GetPIN( uint8_t port , uint8_t pin , uint8_t actividad )
{
	port = port * 8 + 5;

	return ( ( ( GPIO[ port ] >> pin ) & 1 ) == actividad ) ? 1 : 0;
}

uint8_t GetPinValue( uint8_t port , uint8_t pin)
{
	return GetPIN(port,pin,1);
}

void TogglePin( uint8_t port , uint8_t pin)
{
	SetPIN(port,pin,!GetPinValue(port,pin));
}

/********************************************************************************************************/

void GPIO_SetOpenDrainMode( uint8_t portNum, uint8_t pinNum, PIN_OD_MODE odMode)
{
	uint32_t *pPinCon = ((uint32_t *) 0x4002C068UL);

	// Para los Pines P0[27] y P0[28] retorna sin setear nada.
	if(portNum == PORT0 && (pinNum >= 27 && pinNum <= 28))
		return;

	if (odMode == OPEN_DRAIN)
		*(uint32_t *)(pPinCon + portNum) |= (0x01UL << pinNum);

	else
		*(uint32_t *)(pPinCon + portNum) &= ~(0x01UL << pinNum);
}

/********************************************************************************************************/

void SetPINSEL( uint8_t port , uint8_t pin ,uint8_t function)
{
	pinsel_t * auxiliar = NULL;

	auxiliar = &PINSEL[port];

	if( port > 9 ) return; //!No hace nada en caso de error (Bad Port)

	if( pin <= 15 )
	{
		pin *= 2;
		auxiliar->_LOW_PINSEL &= ( ~ ( 3 << pin ) );
		auxiliar->_LOW_PINSEL |= ( function << pin );
	}

	else if( pin < 32 )
	{
		pin %= 16;
		pin *= 2;
		auxiliar->_HIGH_PINSEL &= ( ~ ( 3 << pin ) );
		auxiliar->_HIGH_PINSEL |= ( function << pin );
	}

	else return; //!No hace nada en caso de error (Bad Pin)
}
