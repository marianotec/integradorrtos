/**
 * DOXYGEN COMMENTS
 *
 * @file   FW_initADC.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "FW_LPC1769.h"
#include "FW_adc.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

void InitADC(uint8_t ADC_n)
{
	//!!! LOS PRIMEROS 3 PASOS SON GENERICOS PARA TODOS LOS ADC
	//1.- Prendo el ADC (Periferico)
	PCONP->PCADC = 1;

	//2.- Seteo el clock del ADC a 25MHz:
	PCLKSEL->PCLK_ADC = 0;

	//3.- Seteo el divisor en 1 para que muestree a 200kHz:
	ADC->_CLKDIV = 1;

	//4.- Configuro los pines dependiendo del ADC que deseo utilizar!!! ESTE PASO DEPENDE DEL ADC QUE VOY A USAR!
	switch(ADC_n)
	{
	case 0:
		SetPINSEL(ADC0,FUNCION_1);
		SetDIR(ADC0,ENTRADA);
		break;

	case 1:
		SetPINSEL(ADC1,FUNCION_1);
		SetDIR(ADC1,ENTRADA);
		break;

	case 2:
		SetPINSEL(ADC2,FUNCION_1);
		SetDIR(ADC2,ENTRADA);
		break;

	case 3:
		SetPINSEL(ADC3,FUNCION_1);
		SetDIR(ADC3,ENTRADA);
		break;

	case 4:
		SetPINSEL(ADC4,FUNCION_3);
		SetDIR(ADC4,ENTRADA);
		break;

	case 5:
		SetPINSEL(ADC5,FUNCION_3);
		SetDIR(ADC5,ENTRADA);
		break;

	case 6:
		SetPINSEL(ADC6,FUNCION_2);
		SetDIR(ADC6,ENTRADA);
		break;

	case 7:
		SetPINSEL(ADC7,FUNCION_2);
		SetDIR(ADC7,ENTRADA);
		break;

	default:
		break;
	}

	//5.- Deshabilito las interrupciones:
	ADC->_ADINTEN0 = 0;
	ADC->_ADINTEN1 = 0;
	ADC->_ADINTEN2 = 0;
	ADC->_ADINTEN3 = 0;
	ADC->_ADINTEN4 = 0;
	ADC->_ADINTEN5 = 0;
	ADC->_ADINTEN6 = 0;
	ADC->_ADINTEN7 = 0;

	ADC->_ADGINTEN = 0;

	//6.- Elijo el/los ADC de los que voy a Muestrear:
	ADC->_SEL |= (1<<ADC_n);  /// EJ: 00000010 (elijo el AD0.1)

	//7.- Activo el ADC:
	ADC->_PDN = 1;

	//8.- Selecciono que el ADC muestree solo e inicializo el timer en 0:
	ADC->_START = 0;
	ADC->_EDGE = 0;
	ADC->_BURST = 1;
}
