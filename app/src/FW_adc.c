/**
 * DOXYGEN COMMENTS
 *
 * @file   FW_adc.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "FW_adc.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

int readADC (int ADC_n)
{
	switch (ADC_n)//Me fijo si el ADC que se desea leer, tomo una muestra
	{
	case 0:
		if(ADC0_DONE)
			return ADC0_VALUE;

	case 1:
		if(ADC1_DONE)
			return ADC1_VALUE;

	case 2:
		if(ADC2_DONE)
			return ADC2_VALUE;

	case 3:
		if(ADC3_DONE)
			return ADC3_VALUE;

	case 4:
		if(ADC4_DONE)
			return ADC4_VALUE;

	case 5:
		if(ADC5_DONE)
			return ADC5_VALUE;

	case 6:
		if(ADC6_DONE)
			return ADC6_VALUE;

	case 7:
		if(ADC7_DONE)
			return ADC7_VALUE;

	default:
		return (-1);
	}

	return (-1);
}
