/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Ejemplo con FreeRTOS, USB, LCD 16x2.
 **
 **/

/** \addtogroup TD2 Técnicas Digitales II
 ** @{ */

/** @addtogroup App Aplicación de usuario
 * 	@{
 */

/*
 * Initials     Name
 * ---------------------------
 * PR           Pablo Ridolfi
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20151013 v0.0.1   PR first version
 */

/*==================[inclusions]=============================================*/

#include "board.h"
#include "main.h"
#include "cdc_vcom.h"
#include "FW_infotronic.h"
#include "FW_7SegExp3.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

/*==================[internal data definition]===============================*/

volatile int	keyBuff[5];

/*==================[external data definition]===============================*/

/** @brief cola para recibir caracteres desde el CDC y enviarlos al display */
xQueueHandle qLCD;
xQueueHandle qKEYS;

/** @brief semáforo para sincronizar el envío de un mensaje por el CDC desde
 * la interrupción de un pulsador.
 */
xSemaphoreHandle sem;

/*==================[internal functions definition]==========================*/

/** @brief Inicialización general del hardware
 *
 */
static void initHardware(void)
{
	SystemCoreClockUpdate();
	Board_Init();
	Board_LED_Set(0, false);

	initInfotronic();

	InitADC(1);

	Chip_GPIOINT_Init(LPC_GPIOINT);
	Chip_GPIOINT_SetIntFalling(LPC_GPIOINT, GPIOINT_PORT0, 1 << 18);
	NVIC_EnableIRQ(EINT3_IRQn);
}

/** @brief Tarea que destella el led del stick cada 500ms y prueba STDOUT
 *
 * @param p no utilizado
 */
static void ledTask(void * p)
{
	while (1)
	{
		//Board_LED_Toggle(0);
		TogglePin(RGBR);
		vTaskDelay(500 / portTICK_RATE_MS);
	}
}
//
///** @brief Tarea que inicializa el LCD de 16x2 caracteres y espera
// * caracteres por una cola ajustando el contenido al tamaño del display.
// *
// * @param p no utilizado
// */
//static void lcdTask(void * p)
//{
//	int count = 0;
//	char c;
//
//	vTaskDelay(100);
//	LCD_Init();
//	LCD_GoToxy(0, 0);
//	LCD_Print("Hola!");
//	LCD_GoToxy(0, 0);
//	while(1)
//	{
//		xQueueReceive(qLCD, &c, portMAX_DELAY);
//		if (c == 0x08) {
//			LCD_Init();
//			count = 0;
//		}
//		else if ((c == '\n') || (c == '\r')) {
//			if (count < 16) {
//				LCD_GoToxy(0, 1);
//				count = 16;
//			}
//			else {
//				LCD_GoToxy(0, 0);
//				count = 0;
//			}
//		}
//		else {
//			LCD_SendChar(c);
//			count++;
//			if (count == 16) {
//				LCD_GoToxy(0, 1);
//			}
//			if (count == 32) {
//				LCD_GoToxy(0, 0);
//				count = 0;
//			}
//		}
//	}
//}

static void termTask(void * p)
{
	int adcVal = 0;
	float temp = 0;
	float Vo = 0;
	float resistor = 0;

	while(1)
	{
		adcVal = readADC(1); //Leo del ADC0.1

		if(adcVal >= 0)
		{
			Vo = (VOLTAJE_IN * adcVal)/ADC_MAX_VALUE; //Hallo Diferencia de potencial, es una función lineal

			resistor = (Vo * 10)/(VOLTAJE_IN - Vo); //Hallo la resistencia del termistor, es una funcion Homográfica

			temp = 0.094*(resistor*resistor)-(4.996 * resistor)+66;	//Funcion que calcula la temperatura en base a la resistencia
		}

		Display(temp,DISPLAY_RIGHT);

		vTaskDelay(500 / portTICK_RATE_MS);
	}
}

static void dispTask(void * p)
{
	while(1)
	{
		Display_Barrrido();

		vTaskDelay(4 / portTICK_RATE_MS);
	}
}

#define		PRESSED				0
#define		DEBOUNCE_TRESH		5

static void ledsTask(void * p)
{
	int i;

	while(1)
	{
		xQueueReceive(qKEYS,(void *)keyBuff,portMAX_DELAY);

		if(keyBuff[0] == DEBOUNCE_TRESH)
			TogglePin(LED1);
		if(keyBuff[1] == DEBOUNCE_TRESH)
			TogglePin(LED2);
		if(keyBuff[2] == DEBOUNCE_TRESH)
			TogglePin(LED3);
		if(keyBuff[3] == DEBOUNCE_TRESH)
			TogglePin(LED4);
		if(keyBuff[4] == DEBOUNCE_TRESH)
			TogglePin(BUZZ);
	}
}

static void keysTask(void * p)
{
	int i;

	for(i = 0 ; i < 5 ; i++)	keyBuff[i]=0;

	while(1)
	{
		if(GetPIN(KEY0,PRESSED))	keyBuff[0] += 1;
		else						keyBuff[0]  = 0;

		if(GetPIN(KEY1,PRESSED))	keyBuff[1] += 1;
		else						keyBuff[1]  = 0;

		if(GetPIN(KEY2,PRESSED))	keyBuff[2] += 1;
		else						keyBuff[2]  = 0;

		if(GetPIN(KEY3,PRESSED))	keyBuff[3] += 1;
		else						keyBuff[3]  = 0;

		if(GetPIN(KEY4,PRESSED))	keyBuff[4] += 1;
		else						keyBuff[4]  = 0;

		for(i = 0 ; i >= 0 && i < 5 ; i++)
		{
			if( keyBuff[i] == DEBOUNCE_TRESH )
			{
				xQueueSend(qKEYS,(const void *)keyBuff,portMAX_DELAY);

				vTaskDelay(10 / portTICK_RATE_MS);

				i = -99;
			}
		}

		vTaskDelay(10 / portTICK_RATE_MS);
	}
}

/*==================[external functions definition]==========================*/

/** @brief entry point */
int main(void)
{
	initHardware();

	xTaskCreate(ledTask, (signed const char *)"led", 512, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(cdcTask, (signed const char *)"cdc", 512, 0, tskIDLE_PRIORITY+1, 0);
	//xTaskCreate(lcdTask, (signed const char *)"lcd", 1024, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(termTask,(signed const char *)"term", 512, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(dispTask,(signed const char *)"disp", 512, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(keysTask,(signed const char *)"keys", 1024, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(ledsTask,(signed const char *)"leds", 1024, 0, tskIDLE_PRIORITY+1, 0);

	qLCD = xQueueCreate(32, sizeof(char));
	qKEYS = xQueueCreate(5, sizeof(int));
	vSemaphoreCreateBinary(sem);

	vTaskStartScheduler();

	while(1) __WFI();
}

/** @brief interrupción externa del pulsador SW2 */
void EINT3_IRQHandler(void)
{
	static portBASE_TYPE xSwitchRequired;

	if (Chip_GPIOINT_GetStatusFalling(LPC_GPIOINT, GPIOINT_PORT0) & (1<<18)) {
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIOINT_PORT0, 1<<18);
		xSemaphoreGiveFromISR(sem, &xSwitchRequired);
		TogglePin(RGBG);
	}
	portEND_SWITCHING_ISR(xSwitchRequired);
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/
