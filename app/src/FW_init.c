/**
 * DOXYGEN COMMENTS
 *
 * @file   FW_init.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "FW_infotronic.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

void initInfotronic( void )
{
	//INICIALIZADORES BASE
	Inicializar_Teclado( );
	Inicializar_Relay( ); //TAMBIEN INICIALIZA LOS LEDS
	Inicializar_RGB( );
	Inicializar_Buzz( );
	Inicializar_Expansion_3();

	//WriteUart(1,"hola! Mando un mensaje largo!");

	//InitTimer( 1, 0, 25, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);

	SetPINSEL ( EXPANSION7 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION8 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION9 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION10 , FUNCION_GPIO);

	SetDIR ( EXPANSION7 , SALIDA);
	SetDIR ( EXPANSION8 , SALIDA);
	SetDIR ( EXPANSION9 , SALIDA);
	SetDIR ( EXPANSION10 , SALIDA);

	SetPINSEL ( EXPANSION11 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION12 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION13 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION14 , FUNCION_GPIO);

	SetDIR ( EXPANSION11 , SALIDA);
	SetDIR ( EXPANSION12 , SALIDA);
	SetDIR ( EXPANSION13 , SALIDA);
	SetDIR ( EXPANSION14 , SALIDA);

	SetPINSEL ( LED1 , FUNCION_GPIO);
	SetPINSEL ( LED2 , FUNCION_GPIO);
	SetPINSEL ( LED3 , FUNCION_GPIO);
	SetPINSEL ( LED4 , FUNCION_GPIO);

	SetDIR ( LED1 , SALIDA);
	SetDIR ( LED2 , SALIDA);
	SetDIR ( LED3 , SALIDA);
	SetDIR ( LED4 , SALIDA);
}

void Inicializar_Teclado( void )
{
	SetPINSEL ( KEY0 , FUNCION_GPIO);
	SetPINSEL ( KEY1 , FUNCION_GPIO);
	SetPINSEL ( KEY2 , FUNCION_GPIO);
	SetPINSEL ( KEY3 , FUNCION_GPIO);
	SetPINSEL ( KEY4 , FUNCION_GPIO);

	SetDIR ( KEY0 , ENTRADA);
	SetDIR ( KEY1 , ENTRADA);
	SetDIR ( KEY2 , ENTRADA);
	SetDIR ( KEY3 , ENTRADA);
	SetDIR ( KEY4 , ENTRADA);

	SetPINMODE ( KEY0 , PINMODE_PULLUP);
	SetPINMODE ( KEY1 , PINMODE_PULLUP);
	SetPINMODE ( KEY2 , PINMODE_PULLUP);
	SetPINMODE ( KEY3 , PINMODE_PULLUP);
	SetPINMODE ( KEY4 , PINMODE_PULLUP);
}

void Inicializar_Relay( void )
{
	SetPINSEL ( RELAY1 , FUNCION_GPIO);
	SetPINSEL ( RELAY2 , FUNCION_GPIO);
	SetPINSEL ( RELAY3 , FUNCION_GPIO);
	SetPINSEL ( RELAY4 , FUNCION_GPIO);

	SetDIR ( RELAY1 , SALIDA);
	SetDIR ( RELAY2 , SALIDA);
	SetDIR ( RELAY3 , SALIDA);
	SetDIR ( RELAY4 , SALIDA);
}

void Inicializar_RGB( void )
{
	SetPINSEL ( RGBR , FUNCION_GPIO);
	SetPINSEL ( RGBG , FUNCION_GPIO);
	SetPINSEL ( RGBB , FUNCION_GPIO);

	SetDIR ( RGBR , SALIDA);
	SetDIR ( RGBG , SALIDA);
	SetDIR ( RGBB , SALIDA);

	SetPIN ( RGBR , ACTIVO_BAJO);
	SetPIN ( RGBG , ACTIVO_BAJO);
	SetPIN ( RGBB , ACTIVO_BAJO);
}

void Inicializar_Buzz( void )
{
	SetPINSEL ( BUZZ , FUNCION_GPIO);
	SetDIR ( BUZZ , SALIDA);
	SetPIN ( BUZZ, ACTIVO_BAJO);
}

void Inicializar_Expansion_3( void )
{
	initDisplay7seg();

	//-------------- ENTRADAS DIGITALES (TECLADO 4x2) -----------
	SetPINSEL ( EXPANSION7 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION8 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION9 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION10 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION11 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION12 , FUNCION_GPIO);

	//filas
	SetDIR ( EXPANSION7 , SALIDA);
	SetDIR ( EXPANSION8 , SALIDA);
	SetDIR ( EXPANSION9 , SALIDA);
	SetDIR ( EXPANSION10 , SALIDA);
	//columnas
	SetDIR ( EXPANSION11 , ENTRADA);
	SetDIR ( EXPANSION12 , ENTRADA);
}
