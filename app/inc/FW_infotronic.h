/**
 * DOXYGEN COMMENTS
 * 
 * @file   FW_infotronic.h
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef APP_INC_FW_INFOTRONIC_H_
#define APP_INC_FW_INFOTRONIC_H_

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "FW_LPC1769.h"
#include "FW_adc.h"
#include "FW_7SegExp3.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// CONFIGURATION DEFINEs

// GENERIC DEFINEs

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

#define		ACTIVO_BAJO		0
#define		ACTIVO_ALTO		1

#define		OFF				0
#define		ON				1

/**		EXPANSION PORTS		**/
#define EXPANSION0		PORT2,7
#define EXPANSION1		PORT1,29
#define EXPANSION2		PORT4,28
#define EXPANSION3		PORT1,23
#define EXPANSION4		PORT1,20
#define EXPANSION5		PORT0,19
#define EXPANSION6		PORT3,26
#define EXPANSION7		PORT1,25
#define EXPANSION8		PORT1,22
#define EXPANSION9		PORT1,19
#define EXPANSION10		PORT0,20
#define EXPANSION11		PORT3,25
#define EXPANSION12		PORT1,27
#define EXPANSION13		PORT1,24
#define EXPANSION14		PORT1,21
#define EXPANSION15		PORT1,18
#define EXPANSION16		PORT1,31
#define EXPANSION17		PORT0,24
#define EXPANSION18		PORT0,25
#define EXPANSION19		PORT0,17
#define EXPANSION20		PORT1,31
#define EXPANSION21		PORT0,22
#define EXPANSION22		PORT0,15
#define EXPANSION23		PORT0,16
#define EXPANSION24		PORT2,8
#define EXPANSION25		PORT2,12
#define EXPANSION26		PORT1,31
#define EXPANSION27		PORT1,31

//----------SALIDAS DIGITALES--------------

/**		RELAYs		**/
#define		RELAY1			PORT2,0
#define		RELAY2			PORT0,23
#define		RELAY3			PORT0,21
#define		RELAY4			PORT0,27

/**		LEDS		**/
#define		LED1			RELAY1
#define		LED2			RELAY2
#define		LED3			RELAY3
#define		LED4			RELAY4

/**		BUZZER		**/
#define		BUZZ			PORT0,28

/**		LED RGB		**/
#define		RGBR			PORT2,3
#define		RGBG			PORT2,2
#define		RGBB			PORT2,1

/**		ENTRADAS DIGITALES		**/
//Teclas (teclado 5x1)
#define		KEY0		PORT2,10
#define		KEY1		PORT0,18
#define		KEY2		PORT0,11
#define		KEY3		PORT2,13
#define		KEY4		PORT1,26


/**		EXPANSION 3		**/
#define 	BIT0		EXPANSION0
#define 	BIT1		EXPANSION1
#define 	BIT2		EXPANSION2
#define 	BIT3		EXPANSION3

#define 	DOT			EXPANSION4

// OTHER DEFINEs

/* **************************************************************** */
/*                              IFDEFs                              */

//	Add ifdef's here

/* **************************************************************** */
/*                              MACROs                              */

/**		LED RGB		**/
#define		RED_ON			SetPIN(RGBR, ON)
#define		GREEN_ON		SetPIN(RGBG, ON)
#define		BLUE_ON			SetPIN(RGBB, ON)

#define		RED_OFF			SetPIN(RGBR, OFF)
#define		GREEN_OFF		SetPIN(RGBG, OFF)
#define		BLUE_OFF		SetPIN(RGBB, OFF)

#define		RGB_OFF			RED_OFF;GREEN_OFF;BLUE_OFF
#define		RGB_ON			RED_ON;GREEN_ON;BLUE_ON

/**		BUZZER		**/
#define		BUZZ_ON			SetPIN(BUZZ, ON)
#define		BUZZ_OFF		SetPIN(BUZZ, OFF)

/**		RELAYs		**/
#define		RELAY1_ON		SetPIN(RELAY1, ON)
#define		RELAY2_ON		SetPIN(RELAY1, ON)
#define		RELAY3_ON		SetPIN(RELAY1, ON)
#define		RELAY4_ON		SetPIN(RELAY1, ON)

#define		RELAY1_OFF		SetPIN(RELAY1, OFF)
#define		RELAY2_OFF		SetPIN(RELAY1, OFF)
#define		RELAY3_OFF		SetPIN(RELAY1, OFF)
#define		RELAY4_OFF		SetPIN(RELAY1, OFF)

/**		LEDS		**/
#define		LED1_ON			RELAY1_ON
#define		LED2_ON			RELAY2_ON
#define		LED3_ON			RELAY3_ON
#define		LED4_ON			RELAY4_ON

#define		LED1_OFF		RELAY1_OFF
#define		LED2_OFF		RELAY2_OFF
#define		LED3_OFF		RELAY3_OFF
#define		LED4_OFF		RELAY4_OFF

/* **************************************************************** */
/*                             DATA TYPEs                           */

//	Add data type's here

/* **************************************************************** */
/*                           EXTERNAL GLOBALs                       */

//	Add global variable's here

/* **************************************************************** */
/*                              PROTOTYPEs                          */

void Inicializar_Teclado( void );
void Inicializar_Relay( void );
void Inicializar_RGB( void );
void Inicializar_Display( void );
void Inicializar_Buzz(void);
void Inicializar( void );
void initInfotronic(void);

void Inicializar_Expansion_3( void );

#endif /* APP_INC_FW_INFOTRONIC_H_ */
