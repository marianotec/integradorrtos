/**
 * DOXYGEN COMMENTS
 * 
 * @file   FW_LPC1769.h
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef APP_INC_FW_LPC1769_H_
#define APP_INC_FW_LPC1769_H_

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "stdlib.h"
#include "stdbool.h"
#include "lpc_types.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// CONFIGURATION DEFINEs

// GENERIC DEFINEs

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

#define 	EINT0  		0
#define 	EINT1  		1
#define 	EINT2  		2
#define 	EINT3  		3

#define		PORT0		0
#define		PORT1		1
#define		PORT2		2
#define		PORT3		3
#define		PORT4		4

#define		FUNCION_GPIO	0
#define		FUNCION_1		1
#define		FUNCION_2		2
#define		FUNCION_3		3

#define		MODO_0		0
#define		MODO_1		1
#define		MODO_2		2
#define		MODO_3		3

#define		ENTRADA		0
#define		SALIDA		1

#define		__R					volatile const
#define		__W					volatile
#define		__RW				volatile


//!< GPIO - PORT0
/*	*						*
	*************************
	*		FIODIR			*	0x2009C000
	*************************
	*		RESERVED		*	0x2009C004
	*************************
	*		RESERVED		*	0x2009C008
	*************************
	*		RESERVED		*	0x2009C00C
	*************************
	*		FIOMASK			*	0x2009C010
	*************************
	*		FIOPIN			*	0x2009C014
	*************************
	*		FIOSET			*	0x2009C018
	*************************
	*		FIOCLR			*	0x2009C01C
	*************************
	*						*
	*						*
*/

#define		GPIO		( ( __RW uint32_t * ) 0x2009C000UL )

	//!< ////////////////Registros PINSEL//////////////////////////////
	//!< 00	GPIO (reset value)		01	funcion 1
	//!< 11	funcion 3				10	funcion 2

	//!< //////////////////Registros PINMODE ///////////////////////////
#define		PINMODE		( ( __RW uint32_t * ) 0x4002C040UL )

	//!< ----------- Estados de PINMODE
	//!< 00	Pull Up resistor enable (reset value)		01	repeated mode enable
	//!< 11	Pull Down resistor enable					10	ni Pull Up ni Pull DOwn
	#define		PINMODE_PULLUP 		0
	#define		PINMODE_REPEAT 		1
	#define		PINMODE_NONE 		2
	#define		PINMODE_PULLDOWN 	3


	//0xE000E100UL : Direccion de inicio de los registros de habilitación (set) de interrupciones en el NVIC:

	#define		ISER		( ( uint32_t * ) 0xE000E100UL )
	//0xE000E180UL : Direccion de inicio de los registros de deshabilitacion (clear) de interrupciones en el NVIC:
	#define		ICER		( (  uint32_t * ) 0xE000E180UL )


	//Registros ISER: Para habilitar las Interupciones Se activan con 1 Escribiendo un 0 no hace nada

	#define		ISER0		ISER[0]
	#define		ISER1		ISER[1]
//	#define		ISE_EINT3	ISER[0] |= (0x00000001 << 21)  //ISER0->bit21 pongo un 1 en el bit 21 para habilitar la INT EINT3
//  #define     ISE_EINT2	ISER[0] |= (0x00000001 << 20)  //ISER0->bit20 pongo un 1 en el bit 20 para habilitar la INT EINT2
//  #define     ISE_EINT1	ISER[0] |= (0x00000001 << 19)  //ISER0->bit19 pongo un 1 en el bit 19 para habilitar la INT EINT1
//  #define     ISE_EINT0	ISER[0] |= (0x00000001 << 18)  //ISER0->bit18 pongo un 1 en el bit 18 para habilitar la INT EINT1

	// Registro EXTMODE : Para seleccionar si la ISR Externa activa por flanco ó nivel
	#define		EXTMODE 		( (uint32_t  * ) 0x400FC148 )[0]
//	#define		EXTMODE3_F		EXTMODE[0] |= 0x00000001 << 3  // EINT3 por flanco
//  #define		EXTMODE2_F		EXTMODE[0] |= 0x00000001 << 2  // EINT2 por flanco
//  #define		EXTMODE1_F		EXTMODE[0] |= 0x00000001 << 1  // EINT1 por flanco
//  #define		EXTMODE0_F		EXTMODE[0] |= 0x00000001       // EINT0 por flanco

	// Registro EXTPOLAR : selecciona Polaridad del EXTMODE
	#define    EXTPOLAR        ( (uint32_t  * ) 0x400FC14C )[0]
//  #define    EXTPOLAR3_P      EXTPOLAR[0] |= 0X00000001 << 3 // Flanco ó Nivel Positivo
//  #define    EXTPOLAR2_P      EXTPOLAR[0] |= 0X00000001 << 2 // Flanco ó Nivel Positivo
//  #define    EXTPOLAR1_P      EXTPOLAR[0] |= 0X00000001 << 1 // Flanco ó Nivel Positivo
//  #define    EXTPOLAR0_P      EXTPOLAR[0] |= 0X00000001      // Flanco ó Nivel Positivo


	//Registros ICER: Para deshabilitar las Interupciones Se desactivan con 1 Escribiendo un 0 no hace nada
	//Registros ICER:

	#define		ICER0		ICER[0]
	#define		ICER1		ICER[1]
//	#define		ICE_EINT3	ICER0 |= (0x00000001 << 21) // deshabilito a EINT3
//  #define		ICE_EINT2	ICER0 |= (0x00000001 << 20) // deshabilito a EINT2
//  #define		ICE_EINT1	ICER0 |= (0x00000001 << 19) // deshabilito a EINT1
//  #define		ICE_EINT0	ICER0 |= (0x00000001 << 18) // deshabilito a EINT0


	#define		EXTINT 		( (uint32_t  * ) 0x400FC140UL )[0] // Reg de Flags para limpiar la ISR

//	#define		CLR_EINT3		EXTINT[0] |= 0x00000001 << 3 // bajo el flag de EINT3
//  #define		CLR_EINT2		EXTINT[0] |= 0x00000001 << 2 // bajo el flag de EINT2
//  #define		CLR_EINT1		EXTINT[0] |= 0x00000001 << 1 // bajo el flag de EINT1
//  #define		CLR_EINT0		EXTINT[0] |= 0x00000001      // bajo el flag de EINT0



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Registros de GPIO para usarse como ISR
    // Configuración
    #define      IO0IntEnR  ( (uint32_t  * ) 0x40028090UL ) // Puerto 0 como flanco ascendente
    #define      IO2IntEnR  ( (uint32_t  * ) 0x400280B0UL )//  Puerto 2 como flanco ascendente
    #define      IO0IntEnF  ( (uint32_t  * ) 0x40028094UL )// Puerto 0 como flanco descendente
    #define      IO2IntEnF  ( (uint32_t  * ) 0x400280B4UL )// Puerto 2 como flanco ascendente

   // Estado
    #define     IO0IntStatR  ( (uint32_t  * ) 0x40028084UL ) //Estado de los flags de interr flanco ascendente bits Puerto 0
    #define     IO2IntStatR  ( (uint32_t  * ) 0x400280A4UL ) //Estado de los flags de interr flanco ascendente bits Puerto 2
    #define     IO0IntStatF  ( (uint32_t  * ) 0x40028088UL ) //Estado de los flags de interr flanco descendente bits Puerto 0
    #define     IO2IntStatF  ( (uint32_t  * ) 0x400280A8UL ) //Estado de los flags de interr flanco descendente bits Puerto 2
    #define     IOIntStatus ( (uint32_t  * ) 0x40028080UL ) //Estado de los flags de interr de bits Puerto 2 y Puerto 0

  //Bajo flags de Interr por GPIO
    #define     IO0IntClr  ( (uint32_t  * ) 0x4002808CUL ) //Bajo flags de Interr Puerto 0
	#define     IO2IntClr  ( (uint32_t  * ) 0x400280ACUL ) //Bajo flags de Interr Puerto 2

/** 					Registros PINSEL
 	 	 00	GPIO (reset value)		01	Función 1
		 11	Función 3				10	Función 2
**/
#define 	PINSEL					((pinsel_t *)	0x4002C000)

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// OTHER DEFINEs

/* **************************************************************** */
/*                              IFDEFs                              */

//	Add ifdef's here

/* **************************************************************** */
/*                              MACROs                              */

//	Add macro's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

typedef enum {NOT_OPEN_DRAIN = 0, OPEN_DRAIN = 1} PIN_OD_MODE;

/** Pinsel Struct **/
typedef struct
{
	union
	{
		/** The LOW_PINSEL register controls the functions of the lower half of Port's.
		 * The direction control bit in FIOnDIR register is effective only when the
		 * GPIO function is selected for a pin. For other functions,
		 * the direction is controlled automatically.
		 * */
		__RW uint32_t	_LOW_PINSEL;


		struct
		{
			__RW uint32_t	_Pn_0:2;

			__RW uint32_t	_Pn_1:2;

			__RW uint32_t	_Pn_2:2;

			__RW uint32_t	_Pn_3:2;

			__RW uint32_t	_Pn_4:2;

			__RW uint32_t	_Pn_5:2;

			__RW uint32_t	_Pn_6:2;

			__RW uint32_t	_Pn_7:2;

			__RW uint32_t	_Pn_8:2;

			__RW uint32_t	_Pn_9:2;

			__RW uint32_t	_Pn_10:2;

			__RW uint32_t	_Pn_11:2;

			__RW uint32_t	_Pn_12:2;

			__RW uint32_t	_Pn_13:2;

			__RW uint32_t	_Pn_14:2;

			__RW uint32_t	_Pn_15:2;
		};
	};

	union
	{
		/**
		 * The HIGH_PINSEL register controls the functions of the upper half of Port's.
		 *  The direction control bit in the FIOnDIR register is effective only when the
		 *  GPIO function is selected for a pin.
		 *  For other functions the direction is controlled automatically.
		 */
		__RW uint32_t	_HIGH_PINSEL;

		struct
		{
			__RW uint32_t	_Pn_16:2;

			__RW uint32_t	_Pn_17:2;

			__RW uint32_t	_Pn_18:2;

			__RW uint32_t	_Pn_19:2;

			__RW uint32_t	_Pn_20:2;

			__RW uint32_t	_Pn_21:2;

			__RW uint32_t	_Pn_22:2;

			__RW uint32_t	_Pn_23:2;

			__RW uint32_t	_Pn_24:2;

			__RW uint32_t	_Pn_25:2;

			__RW uint32_t	_Pn_26:2;

			__RW uint32_t	_Pn_27:2;

			__RW uint32_t	_Pn_28:2;

			__RW uint32_t	_Pn_29:2;

			__RW uint32_t	_Pn_30:2;

			__RW uint32_t	_Pn_31:2;
		};
	};
} pinsel_t;

//!< ///////////////////   PCLKSEL   //////////////////////////
//!< Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 -0x400F C1A8 and PCLKSEL1 - 0x400F C1AC) [pag. 56 user manual]
//!< 0x400FC1A8UL : Direccion de inicio de los registros de seleccion de los CLKs de los dispositivos:
#define		PCLKSEL		((pclksel_t*) 0x400FC1A8UL)

typedef struct
{
	__RW uint32_t PCLK_WDT				:2;
	__RW uint32_t PCLK_TIMER0			:2;
	__RW uint32_t PCLK_TIMER1			:2;
	__RW uint32_t PCLK_UART0			:2;
	__RW uint32_t PCLK_UART1			:2;
	__RW uint32_t RESERVED1				:2;
	__RW uint32_t PCLK_PWM1				:2;
	__RW uint32_t PCLK_I2C0				:2;
	__RW uint32_t PCLK_SPI				:2;
	__RW uint32_t RESERVED2				:2;
	__RW uint32_t PCLK_SSP1				:2;//bit 20
	__RW uint32_t PCLK_DAC				:2;
	__RW uint32_t PCLK_ADC				:2;
	__RW uint32_t PCLK_CAN1				:2;
	__RW uint32_t PCLK_CAN2				:2;
	__RW uint32_t PCLK_ACF				:2;
	__RW uint32_t PCLK_QEI				:2;//2nd BYTE
	__RW uint32_t PCLK_GPIOINT			:2;
	__RW uint32_t PCLK_PCB				:2;
	__RW uint32_t PCLK_I2C1				:2;
	__RW uint32_t RESERVED3				:2;//bit 40
	__RW uint32_t PCLK_SSP0				:2;
	__RW uint32_t PCLK_TIMER2			:2;
	__RW uint32_t PCLK_TIMER3			:2;
	__RW uint32_t PCLK_UART2			:2;
	__RW uint32_t PCLK_UART3			:2;
	__RW uint32_t PCLK_I2C2				:2;
	__RW uint32_t PCLK_I2S				:2;
	__RW uint32_t RESERVED4				:2;
	__RW uint32_t PCLK_RIT				:2;
	__RW uint32_t PCLK_SYSCON			:2;//bit 60
	__RW uint32_t PCLK_MC				:2;

} pclksel_t;

//!< ///////////////////   PCONP   //////////////////////////
//!<  Power Control for Peripherals register (PCONP - 0x400F C0C4) [pag. 62 user manual LPC1769]
//!< 0x400FC0C4UL : Direccion de inicio del registro de habilitación de dispositivos:
#define PCONP		((pconp_t *)	0x400FC0C4UL)	/* Registro de Control de Alimentación de Periféricos */


typedef struct
{
	__RW uint32_t RESERVED0:1;
	__RW uint32_t PCTIM0:1;
	__RW uint32_t PCTIM1:1;
	__RW uint32_t PCUART0:1;
	__RW uint32_t PCUART1:1;
	__RW uint32_t RESERVED1:1;
	__RW uint32_t PCPWM1:1;
	__RW uint32_t PCI2C0:1;
	__RW uint32_t PCSPI:1;
	__RW uint32_t PCRTC:1;
	__RW uint32_t PCSSP1:1;//bit 10
	__RW uint32_t RESERVED2:1;
	__RW uint32_t PCADC:1; //<----------ADC
	__RW uint32_t PCCAN1:1;
	__RW uint32_t PCCAN2:1;
	__RW uint32_t PCGPIO:1;
	__RW uint32_t PCRIT:1;
	__RW uint32_t PCMCPWM:1;
	__RW uint32_t PCQEI:1;
	__RW uint32_t PCI2C1:1;
	__RW uint32_t RESERVED3:1;//bit 20
	__RW uint32_t PCSSP0:1;
	__RW uint32_t PCTIM2:1;
	__RW uint32_t PCTIM3:1;
	__RW uint32_t PCUART2:1;
	__RW uint32_t PCUART3:1;
	__RW uint32_t PCI2C2:1;
	__RW uint32_t PCI2S:1;
	__RW uint32_t RESERVED4:1;
	__RW uint32_t PCGPDMA:1;
	__RW uint32_t PCENET:1;//bit 30
	__RW uint32_t PCUSB:1;

} pconp_t;

/* **************************************************************** */
/*                           EXTERNAL GLOBALs                       */

//	Add global variable's here

/* **************************************************************** */
/*                              PROTOTYPEs                          */

void SetPIN( uint8_t port , uint8_t pin , uint8_t estado );

void SetDIR( uint8_t port , uint8_t pin , uint8_t dir );

void SetPINMODE( uint8_t port , uint8_t pin ,uint8_t modo);

void SetPINSEL( uint8_t port , uint8_t pin ,uint8_t function);

void TogglePin( uint8_t port , uint8_t pin);

uint8_t GetPinValue( uint8_t port , uint8_t pin);

uint8_t GetPIN( uint8_t port , uint8_t pin , uint8_t actividad );

void GPIO_SetOpenDrainMode( uint8_t portNum, uint8_t pinNum, PIN_OD_MODE odMode);

#endif /* APP_INC_FW_LPC1769_H_ */
