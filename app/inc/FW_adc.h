/**
 * DOXYGEN COMMENTS
 * 
 * @file   FW_adc.h
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @brief	Este archivo contiene la estructura básica y los defines necesarios para manejar el ADC del LPC1769

The ADC is configured using the following registers:

	1. Power: In the PCONP register (Table 46), set the PCADC bit.
	Remark: On reset, the ADC is disabled. To enable the ADC, first set the PCADC bit,
	and then enable the ADC in the AD0CR register (bit PDN Table 531). To disable the
	ADC, first clear the PDN bit, and then clear the PCADC bit.

	2. Clock: In the PCLKSEL0 register (Table 40), select PCLK_ADC. To scale the clock for
	the ADC, see bits CLKDIV in Table 531.

	3. Pins: Enable ADC0 pins through PINSEL registers. Select the pin modes for the port
	pins with ADC0 functions through the PINMODE registers (Section 8.5).

	4. Interrupts: To enable interrupts in the ADC, see Table 535. Interrupts are enabled in
	the NVIC using the appropriate Interrupt Set Enable register. Disable the ADC
	interrupt in the NVIC using the appropriate Interrupt Set Enable register.

	5. DMA: See Section 29.6.4. For GPDMA system connections, see Table 543.
 */

#ifndef APP_INC_FW_ADC_H_
#define APP_INC_FW_ADC_H_

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "FW_LPC1769.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// CONFIGURATION DEFINEs

#define	VOLTAJE_IN			3.3

// GENERIC DEFINEs

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

#define	ADC_MAX_VALUE		4095		//Tal que el ADC es de 12 bits

#define	ADC0_PORT			PORT0
#define ADC0_PIN			23

#define	ADC1_PORT			PORT0
#define ADC1_PIN			24

#define	ADC2_PORT			PORT0
#define ADC2_PIN			25

#define	ADC3_PORT			PORT0
#define ADC3_PIN			26

#define	ADC4_PORT			PORT1
#define ADC4_PIN			30

#define	ADC5_PORT			PORT1
#define ADC5_PIN			31

#define	ADC6_PORT			PORT0
#define ADC6_PIN			3

#define	ADC7_PORT			PORT0
#define ADC7_PIN			2

#define ADC					((adc_t *)	0x40034000)

//ADC - 12bits - Max: 200kHz
#define	ADC0				ADC0_PORT,ADC0_PIN
#define	ADC1				ADC1_PORT,ADC1_PIN
#define	ADC2				ADC2_PORT,ADC2_PIN
#define	ADC3				ADC3_PORT,ADC3_PIN
#define	ADC4				ADC4_PORT,ADC4_PIN
#define	ADC5				ADC5_PORT,ADC5_PIN
#define	ADC6				ADC6_PORT,ADC6_PIN
#define	ADC7				ADC7_PORT,ADC7_PIN

#define	ADC0_VALUE			ADC->_AD0_RESULT
#define	ADC1_VALUE			ADC->_AD1_RESULT
#define	ADC2_VALUE			ADC->_AD2_RESULT
#define	ADC3_VALUE			ADC->_AD3_RESULT
#define	ADC4_VALUE			ADC->_AD4_RESULT
#define	ADC5_VALUE			ADC->_AD5_RESULT
#define	ADC6_VALUE			ADC->_AD6_RESULT
#define	ADC7_VALUE			ADC->_AD7_RESULT

#define	ADC0_DONE			ADC->_DONE0
#define	ADC1_DONE			ADC->_DONE1
#define	ADC2_DONE			ADC->_DONE2
#define	ADC3_DONE			ADC->_DONE3
#define	ADC4_DONE			ADC->_DONE4
#define	ADC5_DONE			ADC->_DONE5
#define	ADC6_DONE			ADC->_DONE6
#define	ADC7_DONE			ADC->_DONE7

// OTHER DEFINEs

//PCLK_ADC:   Peripheral clock selection for ADC.    Reset: 00           Addres: 0x400FC1A8 (25:24)

//PCADC A/D converter (ADC) power/clock control bit. Note: Clear the PDN bit in the AD0CR before clearing this bit, and set this bit before setting PDN. 0
//address 0x400FC0C4 bit 12

//habilita interrupcion
//ISER0 - 0xE000E100 bit 22
//ISE_ADC ADC Interrupt Enable. See functional description for bit 0.
#define ISE_ADC				(ISER0 |= (0x00000001 << 22))

//deshabilita interrupcion
//ICER0 - 0xE000E180 bit 22
//ICE_ADC ADC Interrupt Disable. See functional description for bit 0.
#define ICE_ADC				(ICER0 |= (0x00000001 << 22))

/* **************************************************************** */
/*                              IFDEFs                              */

//	Add ifdef's here

/* **************************************************************** */
/*                              MACROs                              */

//	Add macro's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

typedef struct
{
	union
	{
		__RW uint32_t	_AD0CR; /*A/D Control Register. The ADCR register must be written to select the
		operating mode before A/D conversion can occur.*/

		struct
		{
			__RW uint32_t	_SEL:8;
			__RW uint32_t	_CLKDIV:8;
			__RW uint32_t	_BURST:1;
			__RW uint32_t	_RESERVADO:4;
			__RW uint32_t	_PDN:1;
			__RW uint32_t	_RESERVADO1:2;

			__RW uint32_t	_START:3;
			/*
			 * _START:
			 *
			 * When the BURST bit is 0, these bits control whether and when an A/D conversion is
started:
000 No start (this value should be used when clearing PDN to 0).
001 Start conversion now.
010 Start conversion when the edge selected by bit 27 occurs on the P2.10 / EINT0 / NMI pin.
011 Start conversion when the edge selected by bit 27 occurs on the P1.27 / CLKOUT / USB_OVRCRn / CAP0.1 pin.
100 Start conversion when the edge selected by bit 27 occurs on MAT0.1. Note that this does not require that the MAT0.1 function appear on a device pin.
101 Start conversion when the edge selected by bit 27 occurs on MAT0.3. Note that it is not possible to cause the MAT0.3 function to appear on a device pin.
110 Start conversion when the edge selected by bit 27 occurs on MAT1.0. Note that this does not require that the MAT1.0 function appear on a device pin.
111 Start conversion when the edge selected by bit 27 occurs on MAT1.1. Note that this does not require that the MAT1.1 function appear on a device pin.
			 */

			__RW uint32_t	_EDGE:1;
			__RW uint32_t	_RESERVADO2:4;
		};
	};

	union
	{
		__RW uint32_t AD0GDR;/*A/D Global Data Register. This register contains the ADC’s DONE bit and
the result of the most recent A/D conversion.*/

		struct
		{
			__RW uint32_t _RESERVED3:4;
			__RW uint32_t _RESULT:12;  //Contiene el valor de la muestra!!!
			__RW uint32_t _RESERVED4:8;
			__RW uint32_t _CHN:3; //Indica en que canal fue tomada!!
			__RW uint32_t _RESERVED5:3;
			__RW uint32_t _OVERRUN:1;
			__RW uint32_t _DONE:1; //Poner en 0 tras leer
		};

	};

	__R uint32_t REGISTRO_RESERVADO;

	union
	{
		__RW uint32_t AD0INTEN;/*A/D Interrupt Enable Register. This register contains enable bits that allow
the DONE flag of each A/D channel to be included or excluded from
contributing to the generation of an A/D interrupt.*/

		struct
		{
			/* Cada interrupcion correspondiente a cada adc se habilita poniendo un 1*/

			__RW uint32_t _ADINTEN0:1;
			__RW uint32_t _ADINTEN1:1;
			__RW uint32_t _ADINTEN2:1;
			__RW uint32_t _ADINTEN3:1;
			__RW uint32_t _ADINTEN4:1;
			__RW uint32_t _ADINTEN5:1;
			__RW uint32_t _ADINTEN6:1;
			__RW uint32_t _ADINTEN7:1;

			__RW uint32_t _ADGINTEN:1;
			/* Reset = 1
			 * 0 Only the individual ADC channels enabled by ADINTEN7:0 will generate interrupts.
			 * 1 Only the global DONE flag in ADDR is enabled to generate an interrupt.
			 */

			__RW uint32_t _RESERVED6:23;

		};
	};

	union
	{
		__R uint32_t AD0DR0;/*(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most
			recent conversion completed on channel "n".*/

		struct
		{
			__R uint32_t _RESERVED7:4;
			__R uint32_t _AD0_RESULT:12;
			__R uint32_t _RESERVED8:14;
			__R uint32_t _AD0_OVERRUN:1;
			__R uint32_t _AD0_DONE:1;
		};
	};

	union
	{
		__R uint32_t AD0DR1;/*(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most
				recent conversion completed on channel "n".*/
		struct
		{
			__R uint32_t _RESERVED9:4;
			__R uint32_t _AD1_RESULT:12;
			__R uint32_t _RESERVED10:14;
			__R uint32_t _AD1_OVERRUN:1;
			__R uint32_t _AD1_DONE:1;
		};
	};

	union
	{
		__R uint32_t AD0DR2;/*(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most
			recent conversion completed on channel "n".*/

		struct
		{
			__R uint32_t _RESERVED11:4;
			__R uint32_t _AD2_RESULT:12;
			__R uint32_t _RESERVED12:14;
			__R uint32_t _AD2_OVERRUN:1;
			__R uint32_t _AD2_DONE:1;
		};
	};

	union
	{
		__R uint32_t AD0DR3;/*(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most
			recent conversion completed on channel "n".*/

		struct
		{
			__R uint32_t _RESERVED13:4;
			__R uint32_t _AD3_RESULT:12;
			__R uint32_t _RESERVED14:14;
			__R uint32_t _AD3_OVERRUN:1;
			__R uint32_t _AD3_DONE:1;
		};
	};

	union
	{
		__R uint32_t AD0DR4;/*(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most
			recent conversion completed on channel "n".*/

		struct
		{
			__R uint32_t _RESERVED15:4;
			__R uint32_t _AD4_RESULT:12;
			__R uint32_t _RESERVED16:14;
			__R uint32_t _AD4_OVERRUN:1;
			__R uint32_t _AD4_DONE:1;
		};
	};

	union
	{
		__R uint32_t AD0DR5;/*(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most
			recent conversion completed on channel "n".*/

		struct
		{
			__R uint32_t _RESERVED17:4;
			__R uint32_t _AD5_RESULT:12;
			__R uint32_t _RESERVED18:14;
			__R uint32_t _AD5_OVERRUN:1;
			__R uint32_t _AD5_DONE:1;
		};
	};

	union
	{
		__R uint32_t AD0DR6;/*(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most
			recent conversion completed on channel "n".*/

		struct
		{
			__R uint32_t _RESERVED19:4;
			__R uint32_t _AD6_RESULT:12;
			__R uint32_t _RESERVED20:14;
			__R uint32_t _AD6_OVERRUN:1;
			__R uint32_t _AD6_DONE:1;
		};
	};

	union
	{
		__R uint32_t AD0DR7;/*(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most
			recent conversion completed on channel "n".*/

		struct
		{
			__R uint32_t _RESERVED21:4;
			__R uint32_t _AD7_RESULT:12;
			__R uint32_t _RESERVED22:14;
			__R uint32_t _AD7_OVERRUN:1;
			__R uint32_t _AD7_DONE:1;
		};
	};

	union
	{
		/**(AD0DRn) A/D Channel "n" Data Register. This register contains the result of the most recent conversion completed on channel "n".*/
		__R uint32_t ADSTAT;

		struct
		{
			__R uint32_t _DONE0:1;
			__R uint32_t _DONE1:1;
			__R uint32_t _DONE2:1;
			__R uint32_t _DONE3:1;
			__R uint32_t _DONE4:1;
			__R uint32_t _DONE5:1;
			__R uint32_t _DONE6:1;
			__R uint32_t _DONE7:1;

			__R uint32_t _OVERRUN0:1;
			__R uint32_t _OVERRUN1:1;
			__R uint32_t _OVERRUN2:1;
			__R uint32_t _OVERRUN3:1;
			__R uint32_t _OVERRUN4:1;
			__R uint32_t _OVERRUN5:1;
			__R uint32_t _OVERRUN6:1;
			__R uint32_t _OVERRUN7:1;

			__R uint32_t _ADINT:1;/* This bit is the A/D interrupt flag. It is one when any of the
				individual A/D channel Done	flags is asserted and enabled to contribute to the A/D
				interrupt via the ADINTEN register.*/

			__R uint32_t _RESERVED23:15;
		};
	};

} adc_t;

/* **************************************************************** */
/*                           EXTERNAL GLOBALs                       */

//	Add global variable's here

/* **************************************************************** */
/*                              PROTOTYPEs                          */

/**
 *
 * @param ADC_n	Indica cual de los ADC se desea utilizar
 */
void InitADC(uint8_t ADC_n);

/**
 * @brief La función toma y retorna el valor del ADC que se desea
 *
 * @param  ADC_n Indica cual de los ADC se desea utilizar
 * @return Devuelve el valor leido en el ADC o en su defecto un -1 (indicando que no se obtuvo ningun valor)
 */
int readADC (int ADC_n);

#endif /* APP_INC_FW_ADC_H_ */
