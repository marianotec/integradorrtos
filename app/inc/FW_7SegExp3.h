/**
 * DOXYGEN COMMENTS
 * 
 * @file   FW_7SegExp3.h
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   4/11/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef APP_INC_FW_7SEGEXP3_H_
#define APP_INC_FW_7SEGEXP3_H_

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "FW_infotronic.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// CONFIGURATION DEFINEs

#define		ARBITRARY_VALUE		111			//VALOR ARBITRARIO PARA QUE COMIENCE APAGADO EL 7SEGMENTOS

#define		DIGITS_FORMAT		""			//FORMATO DE LA STRING DE DIGITOS PARA MOSTRAR TEMPERATURA

#define		DIGIT_QUANTITY		6			//CANTIDAD DE DIGITOS

// GENERIC DEFINEs


// FUNCTIONAL DEFINEs (DO NOT CHANGE)

#define 	SET_DIGIT_OFF		ARBITRARY_VALUE

#define 	DIGIT_QUANTITY_FIX	DIGIT_QUANTITY

#define 	DIGITOS 			DIGIT_QUANTITY

#define 	DISPLAY_LEFT		0
#define 	DISPLAY_RIGHT		1

#define 	CLK_MUX				EXPANSION5
#define 	RST_MUX				EXPANSION6

#define 	MAX_DISPLAY_VALUE	999

#define		INVALID_VALUE		0xF

// OTHER DEFINEs

/* **************************************************************** */
/*                              IFDEFs                              */

//	Add ifdef's here

/* **************************************************************** */
/*                              MACROs                              */

#define 	MUX_7SEG			SetPIN(CLK_MUX,ACTIVO_ALTO);SetPIN(CLK_MUX,ACTIVO_BAJO)
#define		RST_7SEG			SetPIN(RST_MUX,ACTIVO_ALTO);SetPIN(RST_MUX,ACTIVO_BAJO)

//Estas macros son para prender y apagar el punto del 7 segmentos
#define 	DIGIT_DOT_ON		SetPIN(DOT, ACTIVO_ALTO)
#define 	DIGIT_DOT_OFF		SetPIN(DOT, ACTIVO_BAJO)

#define 	DIGIT_VALUE0		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE1		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE2		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_BAJO)
#define		DIGIT_VALUE3		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_BAJO)
#define		DIGIT_VALUE4		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_BAJO)
#define		DIGIT_VALUE5		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE6		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE7		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE8		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_ALTO)
#define 	DIGIT_VALUE9		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_ALTO)
#define 	DIGIT_NOTVALUE  	SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_ALTO)

#define 	CENTENA(X)			(X/100)
#define 	DECENA(X)			((X%100)/10)
#define 	UNIDAD(X)			(X%10)

#define 	DISPLAY_DIGIT_OFF(X)	SetDisplayDigit(ARBITRARY_VALUE,X)

/* **************************************************************** */
/*                             DATA TYPEs                           */

//	Add data type's here

/* **************************************************************** */
/*                           EXTERNAL GLOBALs                       */

volatile int BufferDisplay[DIGITOS];

/* **************************************************************** */
/*                              PROTOTYPEs                          */

void initDisplay7seg (void);

void setDIGIT(char digito);

void Display_Barrrido(void);

void Display(uint16_t Val, uint8_t dsp);

void SetDisplayDigit(uint8_t Val, uint8_t digit);

#endif /* APP_INC_FW_7SEGEXP3_H_ */
